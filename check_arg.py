from numpy import load

#Check argument of data NPZ
fname=["sim_O4_15_oct_unlensed_dets","sim_O4_15_oct_unlensed_dets_HL_netsnr7","unlensed_inj_data_converted"]
#Not for injection : unlensed_inj_data,	sub_unlensed_refined,
for i in range(len(fname)):
	data = load(fname[i]+'.npz')
	lst = data.files
	print("\n", fname[i] ,"\n")
	for item in lst:
    		print(item)