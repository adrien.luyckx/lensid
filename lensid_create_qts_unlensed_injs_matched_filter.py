import pylab
import pycbc.noise
import pycbc.psd
from pycbc.waveform import get_td_waveform
from pycbc.detector import Detector
from numpy.core.defchararray import add
import os
import numpy as np
import pandas as pd
import sys
import qt_utils as qtils
import argparse

#Added package
from pycbc import frame
from pycbc import types
#Matched filtering
from pycbc.filter import resample_to_delta_t, highpass
from pycbc.psd import interpolate, inverse_spectrum_truncation
from pycbc.filter import matched_filter



#This code inject simulated signal(parameters contained in -infile)
#into noise : generated from the psd seen here (highpower - https://pycbc.org/pycbc/latest/html/psd.html) or from text file (see inj_psds_HLV in qt_utils)
#It stores the resulting QTs into -odir
def main():
    parser = argparse.ArgumentParser(
        description='This is stand alone code for generating QTs for unlensed injection dataset')

    parser.add_argument(
        '-odir',
        '--odir',
        help='Output directory',
        default='check')
    parser.add_argument(
        '-start',
        '--start',
        type=int,
        help='unlensed inj start index',
        default=0)
    parser.add_argument(
        '-whitened',
        '--whitened',
        help='1/0 default 1',
        type=int,
        default=1)
    parser.add_argument(
        '-n',
        '--n',
        type=int,
        help='no. of unlensed injs',
        default=0)
    parser.add_argument(
        '-mode',
        '--mode',
        type=int,
        help='enter no : 1. default \t 2. test',
        default=1)
    parser.add_argument(
        '-infile',
        '--infile',
        help='.npz unlensed injs file path to load tags from',
        required=1)
    parser.add_argument(
        '-psd_mode',
        '--psd_mode',
        type=int,
        help='enter no :1. analytical \t 2.load from files \t 3. load from gwf \t 4.no psd but add noise directly via strain into signal',
        default=1)
    parser.add_argument(
        '-single_det',
        '--single_det',
        help='optional, if produce QT for single det, provide det name: H1,L1,V1',
        default=0)
    parser.add_argument(
        '-asd_dir',
        '--asd_dir',
        help='optional, give directory where psd files are in format H1.txt, L1.txt, V1.txt',
        default=None)
    parser.add_argument(
        '-qrange',
        '--qrange',
        type=int,
        help='1. old qrange 3,7 for m1>60 and 4,10 otherwise. 2. default  is 3,30',
        default=2)
    args = parser.parse_args()
    
    #Show argument used
    print('\n Arguments used:- \n')
    for arg in vars(args):
        print(arg, ': \t', getattr(args, arg))
    #Load a analytical psd
    if args.psd_mode == 1:
        psd_mode = 'analytical'
        psd_H, psd_L, psd_V = qtils.inj_psds_HLV(psd_mode=psd_mode)
        psd_dict={'H1':psd_H,'L1':psd_L,'V1':psd_V}
    #Load a psd from a directory
    elif args.psd_mode == 2:
        psd_mode = 'load'
        psd_H, psd_L, psd_V = qtils.inj_psds_HLV(
            psd_mode=psd_mode, asd_dir=args.asd_dir)
        psd_dict={'H1':psd_H,'L1':psd_L,'V1':psd_V}
    elif args.psd_mode == 3:
        #load my data
        gwf_file=["Noise/H-H1_GWOSC_O3b_4KHZ_R1-1256689664-4096.gwf","Noise/L-L1_GWOSC_O3b_4KHZ_R1-1256689664-4096.gwf","Noise/V-V1_GWOSC_O3b_4KHZ_R1-1256689664-4096.gwf"]
        channel=["H1:GWOSC-4KHZ_R1_STRAIN","L1:GWOSC-4KHZ_R1_STRAIN","V1:GWOSC-4KHZ_R1_STRAIN"]
        data_adri=[[],[],[]]
        estimated_psd =[[],[],[]]
        noise_H, noise_L, noise_V = frame.read_frame(gwf_file[0],channel[0]),frame.read_frame(gwf_file[1],channel[1]),frame.read_frame(gwf_file[2],channel[2])
        # Estimate the PSD
        delta_t = 1.0 / 4096
        seg_len = int(16 / delta_t)
        seg_stride = int(seg_len / 2)
        tsamples = int(128 / delta_t)
        psd_H, psd_L, psd_V = pycbc.psd.welch(noise_H,seg_len=seg_len,seg_stride=seg_stride), pycbc.psd.welch(noise_L,seg_len=seg_len,seg_stride=seg_stride), pycbc.psd.welch(noise_V,seg_len=seg_len,seg_stride=seg_stride)
          #which type of window   -> Hann?
          #           and method  -> Welch, Bartlett?
        psd_dict={'H1':psd_H,'L1':psd_L,'V1':psd_V}
    elif args.psd_mode == 4:
        #load my data
        gwf_file=["Noise/H-H1_GWOSC_O3b_4KHZ_R1-1256689664-4096.gwf","Noise/L-L1_GWOSC_O3b_4KHZ_R1-1256689664-4096.gwf","Noise/V-V1_GWOSC_O3b_4KHZ_R1-1256689664-4096.gwf"]
        channel=["H1:GWOSC-4KHZ_R1_STRAIN","L1:GWOSC-4KHZ_R1_STRAIN","V1:GWOSC-4KHZ_R1_STRAIN"]
        data_adri=[[],[],[]]
        estimated_psd =[[],[],[]]
        noise_H, noise_L, noise_V = frame.read_frame(gwf_file[0],channel[0]),frame.read_frame(gwf_file[1],channel[1]),frame.read_frame(gwf_file[2],channel[2])
        noise_dict={'H1':noise_H,'L1':noise_L,'V1':noise_V}
    else:
        print('invalid psd_mode choice')
        
    duration = 64
    
    if args.single_det ==0:
        dets = ['H1','L1','V1']
    else:
        dets = [args.single_det]    
    data = np.load(args.infile,allow_pickle=True)
    #print("parameters of the data : ",data.__dict__)
    if args.mode == 1:
        lost_ids = []
    elif args.mode == 2:
        lost_ids = np.array([55, 57, 68, 76, 182, 207, 225, 277])
    else:
        print('mode not found')
    
    ntot = data['mass_1'].shape[0]
    if args.n == 0:
        args.n = ntot
    print("Maximum number of injection possible : ", ntot)

    whitened = args.whitened

    odir = args.odir

    if not os.path.exists(odir):
        os.makedirs(odir)
    try:
        for det in dets:
            os.makedirs(odir+"/"+det+"/"+str(args.psd_mode))
    except BaseException:
        print(odir, ' already exists. saving Qtransforms to it.')

    for count, i in enumerate(range(args.start, args.start + args.n)):
	#Q-Transform part
        if count>=7:
            i=i+1
        print(i)
        if args.qrange == 1:
            if data["mass_1"][i] < 60:
                q = qtils.q_msmall
            else:
                q = qtils.q_mlarge
        else:
            q = qtils.q_wide
        hp, hc = get_td_waveform(approximant="IMRPhenomXPHM", mass1=data["mass_1"][i], mass2=data['mass_2'][i], inclination=data["theta_jn"][i], delta_t=1.0 / 2**12, f_lower=15, f_higher=1000, distance=data["luminosity_distance"][i], coa_phase=data['phase'][i],spin_1x=data['spin_1x'][i],spin_1y=data['spin_1y'][i],spin_1z=data['spin_1z'][i],spin_2x=data['spin_2x'][i],spin_2y=data['spin_2y'][i],spin_2z=data['spin_2z'][i])
        # flower=30 works without whitening
        print(data["mass_1"][i], data['mass_2'][i])

        end_time = data["geocent_time"][i]
        declination = data['dec'][i]
        right_ascension = data["ra"][i]
        polarization = data["psi"][i]
        hp.start_time += end_time
        hc.start_time += end_time
        if args.psd_mode == 4:
            for det in dets:
                pycbc_det=Detector(det)
                signal = pycbc_det.project_wave(
                    hp, hc, right_ascension, declination, polarization)
                #print(signal,hp)
                #print(len(signal),len(noise_dict[det]))
                if len(signal)<len(noise_dict[det]): #check good size
                    indice_f=len(noise_dict[det])+1  #set initial value
                    while  indice_f > len(noise_dict[det]): #take final indice in data
                        indice_i = np.random.randint(len(noise_dict[det]), size=1)
                        indice_f = indice_i+len(signal)
                        #print(indice_i,indice_f,len(signal),len(noise_dict[det]))
                    indices=np.arange(indice_i, indice_f, 1,dtype=int) #create indices used
                    ts=types.TimeSeries(np.take(noise_dict[det], indices),delta_t=noise_dict[det].delta_t) #truncate noise in the indices & transform numpy array into a pycbc timeseries
                    ts.start_time = signal.start_time + 5
                    noise_signal = ts.add_into(signal)
                    #print(signal,noise_dict[det],noise_signal)
                else:
                    print("More signal than noise, reduce signal length or take more noise")
            
                fname = str(data['event_tag'][i])
                #print(noise_signal.end_time)
                if whitened == 1:
                    noise_signal_whitened = noise_signal.whiten(4, 4, low_frequency_cutoff=10) * 1e-21
                    noise_signal=noise_signal_whitened.time_slice(signal.end_time - 6, signal.end_time + 2)
                    fname = fname + '-whitened'
                else:
                    noise_signal=noise_signal.time_slice(signal.end_time - 6, signal.end_time + 2)
                power = qtils.plot_qt_from_ts(
                    noise_signal, data["geocent_time"][i], q, outfname=odir+"/"+det+"/"+str(args.psd_mode)+"/"+str(det)+"_"+fname,title=str(data["mass_1"][i])+" "+str(data['mass_2'][i]))
                #print(det)

                #Matched filtering part
                strain = resample_to_delta_t(highpass(noise_signal, 15.0), 1/2**12)
                conditioned = strain#.crop(3, 3)
                psd = conditioned.psd(3)
                psd = interpolate(psd, conditioned.delta_f)
                psd = inverse_spectrum_truncation(psd, int(3 * conditioned.sample_rate),
                                          low_frequency_cutoff=15)
                signal.resize(len(conditioned))
                template = signal.cyclic_time_shift(float(signal.start_time))
                snr = matched_filter(template, conditioned,
                             psd=psd, low_frequency_cutoff=20)
                pylab.figure(figsize=[10, 4])
                pylab.plot(snr.sample_times, abs(snr))
                pylab.ylabel('Signal-to-noise')
                pylab.xlabel('Time (s)')
                pylab.xlim(data["geocent_time"][i]- 1.0, data["geocent_time"][i] + 2.0)

                peak = abs(snr).numpy().argmax()
                snrp = snr[peak]
                time = snr.sample_times[peak]
                pylab.savefig(odir+"/"+det+"/"+str(args.psd_mode)+"/"+str(det)+"_"+fname+"_SNR" + ".png", bbox_inches="tight", pad_inches=0)
                #print("We found a signal at {}s with SNR {}".format(abs(snrp)))
        else:
            for det in dets:
                pycbc_det=Detector(det)
                signal = pycbc_det.project_wave(
                    hp, hc, right_ascension, declination, polarization)
                noise_signal = qtils.inject_noise_signal(
                    signal, psd_dict[det], duration=duration, whitened=whitened)
            
                fname = str(data['event_tag'][i])
                if whitened == 1:
                    fname = fname + '-whitened'
                power = qtils.plot_qt_from_ts(
                    noise_signal, data["geocent_time"][i], q, outfname=odir+"/"+det+"/"+str(args.psd_mode)+"/"+str(det)+"_"+fname,title=str(data["mass_1"][i])+" "+str(data['mass_2'][i]))
                #print(det)
                
                #Matched filtering part
                strain = resample_to_delta_t(highpass(noise_signal, 15.0), 1/2**12)
                conditioned = strain#.crop(3, 3)
                psd = conditioned.psd(3)
                psd = interpolate(psd, conditioned.delta_f)
                psd = inverse_spectrum_truncation(psd, int(3 * conditioned.sample_rate),
                                          low_frequency_cutoff=15)
                signal.resize(len(conditioned))
                template = signal.cyclic_time_shift(float(signal.start_time))
                snr = matched_filter(template, conditioned,
                             psd=psd, low_frequency_cutoff=20)
                pylab.figure(figsize=[10, 4])
                pylab.plot(snr.sample_times, abs(snr))
                pylab.ylabel('Signal-to-noise')
                pylab.xlabel('Time (s)')
                pylab.xlim(data["geocent_time"][i]- 1.0, data["geocent_time"][i] + 1.0)

                peak = abs(snr).numpy().argmax()
                snrp = snr[peak]
                time = snr.sample_times[peak]
                pylab.savefig(odir+"/"+det+"/"+str(args.psd_mode)+"/"+str(det)+"_"+fname+"_SNR" + ".png", bbox_inches="tight", pad_inches=0)
                #print("We found a signal at {}s with SNR {}".format(abs(snrp)))

#in order to launch script
if __name__ == "__main__":
    main()
